#image NodeJS LongTimeSupport 
FROM node:18-alpine

#fixe le fuseau horaire
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#met à jour tous les modules de l'image.
RUN apk update && apk upgrade \
    && apk --no-cache add tar curl tini bash \
    && apk --no-cache add --virtual devs python3 git openssh make gcc build-base  wget \
    #&& apk del devs \
    && rm -rf /tmp/npm*

#installation de PM2 
RUN npm install pm2 -g

#cree le repertoire de destination de l'application
RUN mkdir /var/www
WORKDIR /var/www/

#copie les fichiers du repository de gitLab via le token d'authentification
#ARG FCV_VERSION
#RUN git clone "https://deployTool:CdPvDm1mAAm9StPYdMML@gitlab.com/AjS_clemdpt/fcv_back.git" --branch 1.0.0 . 
COPY src .
RUN npm install

#indique le(s) repertoire(s) qu'on partage
VOLUME /app/log
VOLUME /var/www

#indique le port que l'on expose
EXPOSE 80

#command qui lance pm2. Le manager process
CMD ["pm2-runtime", "start","/var/www/src/server.js","--name","fcv_api"]


# compile une image 
#docker build --pull -t fcvback:1.0.0 .
#docker build --pull --rm -f "Dockerfile" -t fcv_back:0.1.0 "."

# supprime l'image
#docker image rm -f fcv_back:1.0.0

#supprime l'instance
#docker  rm -f fcv_back_i010

#run un container
#docker run --rm -it -p 8080:8080 gcr.io/fcv-back-1/backend

# construit un container et le lance 
#docker run -p 80:80 -p 443:443 -d --name fcv_back_i010 fcv_back:0.1.0

#permet de se connecter au container lancé
#docker exec -p 80:80 -p 443:443 -it fcv_back_i010 /bin/sh

#permet de consulter les logs 
#docker logs fcv_back_i010

#docker run -p 80:80 -p 443:443 -it --name fcv_back_i010 /bin/sh
#docker run -p 80:80 -p 443:443 -it --name fcv_back:0.1.0 /bin/sh
#docker run -p 80:80 -p 443:443 --name fcv_back_i010 fcv_back:0.1.0
#docker exec -it fcv_back_i010 /bin/sh

#docker start fcv_back_i010
#docker stop fcv_back_i010
#netstat -tanplu
