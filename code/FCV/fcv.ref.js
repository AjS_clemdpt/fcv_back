export default {
    models: {
        User: {
            model: {
                civilite: { 
                    datatype: {type: "CHOICE", values : [ "MR",'MME',"MLLE"], },
                },
                entreprise: { 
                    datatype : { type: "STRING" },
                },
                tel1: { 
                    datatype: {type: "PHONE", }, 
                },
                tel2: { 
                    datatype : { type: "PHONE" },
                },
            },
        },
        Client: {
            model: {
                raisonSocial: { datatype : { type: "STRING" },},
                description: { datatype : { type: "TEXT" },},
                deleted: { 
                    datatype : { type: "BOOLEAN" },
                    infos:{
                        label:`Supprimé`,
                    },
                },
                origineCreation: { 
                    datatype : { 
                        type: "CHOICE", 
                        values: ["MailingInternet", "contactTel", "Parrainage", "Prescripteur", "MailingBP", "PuplipostageMairie", "PuplipostageEcole", "Fax"], 
                        //other: true 
                    },
                },
            },
            associate: {
                contacts:{ assignement : "hasMany", modelName : "Contact", 
                  sqlOptions:{foreignKey: 'clientId', onDelete: "CASCADE",},
                  infos:{ 
                    hint:'Liste des constact(s) associé(s) au client',
                    label:`Contact(s) associé(s)`,
                  },
                },
                suiviPar:{ assignement : "belongsTo", modelName : "User", 
                  sqlOptions:{onDelete: "SET NULL" },
                  infos:{ 
                    hint:'Liste des utilisateur(s)',
                    label:`Suivi par`,
                  },
                },
                adresses:{ assignement : "hasMany", modelName : "Adresse", 
                  sqlOptions:{foreignKey: 'clientId', onDelete: "CASCADE"},
                  infos:{ 
                    hint:'Liste des adresse(s) associée(s) au client',
                    label:`adresse(s) associée(s)`,
                  },
                },
                chantiers:{ assignement : "hasMany", modelName : "Chantier", 
                  sqlOptions:{foreignKey: 'clientId'},
                  infos:{ 
                    hint:'Liste des chantier(s) associé(s) au client',
                    label:`chantier(s) associé(s)`,
                  },
                },
                
            },
        },
        Contact: {
            model: {
                nom: { datatype : { type: "CODE" },},
                prenom: { datatype : { type: "CODE" },},
                email: { datatype : { type: "EMAIL" },},
                fonction: { datatype : { type: "CODE" },},
                civilite: { datatype : { type: "CHOICE", values: ["MR", "MME", "MLLE"], },},
                tel1: { datatype : { type: "PHONE" },},
                tel2: { datatype : { type: "PHONE" },},
                isPrincipal: {datatype : { type: "BOOLEAN" },},
            },
        },
        Adresse: {
            model: {
                ligne1: { datatype : { type: "STRING" },},
                ligne2: { datatype : { type: "STRING" },},
                codePostal: { datatype : { type: "STRING", length:5, patern: /[\d]{5}/i },},
                ville: { datatype : { type: "STRING" },},
                codeIsoPays: { datatype : { type: "STRING", length:2 },},
                type: { datatype : { type: "CHOICE", values: ["Siege", "Filialle"] },},
                isAddFacturation: { datatype : { type: "BOOLEAN" },},
            },
        },
        GabaritEmail: {
            model: {
                code: { datatype : { type: "CODE" },},
                sujet: { datatype : { type: "STRING" },},
                contenu: { datatype : { type: "TEXT" },},
            },
        },
        Chantier: {
            model: {
                //Général
                montantHT: { datatype : { type: "CURRENCY" },},
                montantTTC: { datatype : { type: "CURRENCY" },},
                tempsPrevu: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                tempsReel: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                typeChantier: { datatype : { type: "CHOICE", values: ["standard", "SAV"] },},
                commentaire: { datatype : { type: "LABEL", limit: 255 },},
                debPosePrevu: { datatype : { type: "DATE" },},
                debPoseReel: { datatype : { type: "DATE" },},
                finPoseReel: { datatype : { type: "DATE" },},
                commentaireSAV: { datatype : { type: "STRING", length:1000 },},
                numDevis: { datatype : { type: "CODE" },},
                
                //Dimension Chantier
                dimPoseHauteurMax: { datatype : { type: "CHOICE", values: ["2", "2.25", "2,5", "2.75", "3", "3.25", "3.5", "sup3.5"] },},
                dimEchafaudageHauteurMax: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                dimNacelleHauteurMax: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                dimNacelleResistanceSol: { datatype : { type: "BOOLEAN" },},
                dimAccesLargeurMax: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                dimAccesHauteurMax: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                dimNacelleType: { datatype : { type: "CHOICE", values: ["Ciseaux", "BrasArticule"] },},
                //Environnement
                envEncombrement: { datatype : { type: "CHOICE", values: ["Libre", "Bureaux", "Armoire"], other: true },},
                envLocalisation: { datatype : { type: "CHOICE", values: ["Interieur", "Exterieur", "Toiture"], other: true },},
                envAccessibilite: { datatype : { type: "CHOICE", values: ["Facile", "Moyen", "Difficile", "TresDifficile"] },},
                envSolType: { datatype : { type: "CHOICE", values: ["Carrelage", "Terre", "Enrobe", "Parquet", "Ciment_beton"], other: true },},
                // infos complémentaires
                infoOrientation: { datatype : { type: "CHOICE", values: ["Nord", "Nord-Est", "Est", "Sud-Est", "Sud", "Sud-Ouest", "Ouest", "Nord-Ouest"] },},
                infoStoreDemontage: { datatype : { type: "BOOLEAN", allowNull: true },},
                infoStoreEvacutation: { datatype : { type: "BOOLEAN", allowNull: true },},
                infoStoreDecapage: { datatype : { type: "BOOLEAN" },},
                infoDistanceEntrepriseChantier: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                infoDemandeAcces: { datatype : { type: "STRING" },},
                //Sécurité
                secuPPSPS: { datatype : { type: "BOOLEAN", allowNull: true },},
                secuConsigneParticuliere   : { datatype : { type: "STRING" },},
                secuTravailMatin: { datatype : { type: "BOOLEAN", allowNull: true },},
                secuTravailSoir: { datatype : { type: "BOOLEAN", allowNull: true },},
                //Pose
                poseCommentaire: { datatype : { type: "TEXT" },},
                poseTempsPasse: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                //Compta
                comptaEnvoiFacture: { datatype : { type: "DATE" },},
                comptaReceptionPaiement: { datatype : { type: "DATE" },},
                
            },
            associate: {
                suiviPar:{ assignement : "belongsTo", modelName : "User", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des utilisateurs',
                        label:`Suivi par`,
                    },
                },
                poseur:{ assignement : "belongsTo", modelName : "User", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des utilisateurs',
                        label:`Resp. Pose`,
                    },
                },
                contact:{ assignement : "belongsTo", modelName : "Contact", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des contacts',
                        label:`Contact`,
                    },
                },
                chantierParent:{ assignement : "belongsTo", modelName : "Chantier", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des chantiers du client',
                        label:`Chantier Parent`,
                    },
                },
                adrFacturation:{ assignement : "belongsTo", modelName : "Adresse", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des adresses',
                        label:`Adresse de facturation`,
                    },
                },
                adrChantier:{ assignement : "belongsTo", modelName : "Adresse", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des adresses',
                        label:`Adresse du chantier`,
                    },
                },
                client:{ assignement : "belongsTo", modelName : "Client", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des clients',
                        label:`Client`,
                    },
                },
                etatsChantier:{ assignement : "hasMany", modelName : "HistoriqueEtatChantier", 
                    sqlOptions:{ foreignKey: 'chantierId', onDelete: "SET NULL" },
                    infos:{ 
                        hint:'etat du chantier en cours',
                        label:`Etat du chantier en cours`,
                    },
                },
                mediaList:{ assignement : "hasMany", modelName : "MediaDescription", 
                  sqlOptions:{foreignKey: 'chantierId', onDelete: "CASCADE"},
                  infos:{ 
                    hint:'Liste des médias associés(s) au chantier',
                    label:`medias disponibles`,
                  },
                },
                metrages:{ assignement : "hasMany", modelName : "Metrage", 
                  sqlOptions:{foreignKey: 'chantierId', onDelete: "CASCADE"},
                  infos:{ 
                    hint:'Liste des métrages associés(s) au chantier',
                    label:`métrages`,
                  },
                },
            },
        },
        Metrage: {
            model: {
                emplacement:{ datatype : { type: "STRING" },},
                quantiteDevis: { datatype : { type: "INTEGER" },},
                quantiteProd: { datatype : { type: "INTEGER" },},
                dateValidation: { datatype : { type: "DATE" },},
                caracteristiques: { datatype : { type: "JSONB" },},
            },           
            associate: {
                chantier:{ assignement : "belongsTo", modelName : "Chantier", sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'Liste des chantiers',
                        label:`Chantier`,
                    },
                },
                produit:{ assignement : "belongsTo", modelName : "Produit", sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'Liste des produits',
                        label:`Produit`,
                    },
                },
                mediaList:{ assignement : "hasMany", modelName : "MediaDescription", 
                  sqlOptions:{foreignKey: 'metrageId', otherKey: 'chantierId', onDelete: "CASCADE"},
                  infos:{ 
                    hint:'Liste des médias associés(s) au chantier',
                    label:`medias disponibles`,
                  },
                },
            },
        },
        HistoriqueEtatChantier: {
            model: {
            },
            associate: {
                chantier:{ assignement : "belongsTo", modelName : "Chantier", sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'Liste des chantiers',
                        label:`Chantier`,
                    },
                },
                etatChantier:{ assignement : "belongsTo", modelName : "EtatChantier", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des etats chantier',
                        label:`Etat des Chantier`,
                    },
                },
                
            },
        },
        EtatChantier: {
            model: {
                code: { datatype : { type: "CODE" },},
                description: { datatype : { type: "TEXT" },},
                couleur: { datatype : { type: "STRING" },},
                ordre: { datatype : { type: "INTEGER" },},
            },
        },
        Cout: {
            model: {
                montant: { datatype : { type: "DECIMAL", precision : 5, scale: 2 },},
                type: { datatype : { type: "CHOICE", values: ["Commercial", "Client", "Poseur", "Chantier", "Autre"] },},
                commentaire: { datatype : { type: "TEXT" },},
            },
            associate: {
                chantier:{ assignement : "belongsTo", modelName : "Chantier", sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'Liste des chantiers',
                        label:`Chantier`,
                    },

                },
            },
        },
        MediaDescription: {
            model: {
                name: { datatype : { type: "CODE" },},
                description: { datatype : { type: "TEXT" },},
                taille: { datatype : { type: "INTEGER" },},
                duree: { datatype : { type: "INTEGER" },},
                mime: { datatype : { type: "CODE" },},
                categorie : { datatype : { type: "CODE" },},
                uniqueId: { datatype : { type: "UUID" },},
            },
            associate: {
                chantier:{ assignement : "belongsTo", modelName : "Chantier", sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'chantier associé',
                        label:`Chantier`,
                    },
                },
                metrage:{ assignement : "belongsTo", modelName : "Metrage", sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'métrage associé',
                        label:`Métrage`,
                    },
                },
                media:{ assignement : "belongsTo", modelName : "Media", sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'Media',
                        label:`Media`,
                    },
                },
                etatChantier:{ assignement : "belongsTo", modelName : "EtatChantier", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'etat du chantier',
                        label:`Etat des Chantiers`,
                    },
                },
            },

        },
        Media: {
            model: {
                file: { datatype : { type: "BLOB" },},
            },
            /*
            associate:{
                mediaDesc:{ assignement : "belongsTo", modelName : "MediaDescription", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Media Description',
                        label:`Media Description`,
                    },
                },
            }
            */
        },
        Affectation: {
            model: {
                demandeLe: { datatype : { type: "DATE" },},
                reponseLe: { datatype : { type: "DATE" },},
                etat: { datatype : { type: "CHOICE", values: ["EnAttente", "Accepte", "Refuse", "Retire"] },},
                commentaire: { datatype : { type: "TEXT" },},
                //poseur          : { datatype : { type: DataTypes.Identifier },
                //commentairePar  : { datatype : { type: DataTypes.Identifier },
            },
            associate: {
                chantier:{ assignement : "belongsTo", modelName : "Chantier", sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'Liste des chantiers',
                        label:`Chantier`,
                    },
                },
                poseur:{ assignement : "belongsTo", modelName : "User", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des poseurs',
                        label:`Resp. Pose`,
                    },
                },
                auteurCom:{ assignement : "belongsTo", modelName : "User", sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'Liste des utilisateurs',
                        label:`Auteur du commentaire`,
                    },
                },
            },

        },
        
        Produit: {
            model: {
                code: { datatype : { type: "CODE" },},
                categorie: { datatype : {type: "CHOICE", values : [ "Film",'BSO',"Enrouleur Ext","Rideau","Générique"], },},
                type : { datatype : { type: "CODE" },},
                description: { datatype : { type: "TEXT" },},
                visuel: { datatype : { type: "STRING" },},
                deleted: { datatype : { type: "BOOLEAN" },},
                caracteristiques : { datatype : { type: "JSONB" },},
                formTmpl : { datatype : { type: "JSONB" },},
                resumeTmpl : { datatype : { type: "STRING" },},
            },
            associate: {
                fournisseur:{                     
                    assignement : "belongsTo", modelName : "Fournisseur", 
                    sqlOptions:{ onDelete: "SET NULL" },
                    infos:{ 
                        hint:'fournisseur du produit',
                        label:`fournisseur du produit`,
                    },
                },
            },
        },

        Fournisseur: {
            model: {
                raisonSocial: { datatype : { type: "STRING" },},
                description: { datatype : { type: "TEXT" },},
                tel: { datatype : { type: "PHONE" },},
                deleted: { datatype : { type: "BOOLEAN" },},
                email: { datatype : { type: "EMAIL" },},
            },
            associate: {
                adresse:{ 
                    assignement : "belongsTo", modelName : "Adresse", 
                    sqlOptions:{ onDelete: "CASCADE" },
                    infos:{ 
                        hint:'adresse du fournisseur',
                        label:`adresse du fournisseur`,
                    },
                },
                contacts:{ 
                    assignement : "hasMany", modelName : "Contact", 
                    sqlOptions:{foreignKey: 'fourniseurId', onDelete: "CASCADE",},
                    infos:{ 
                    hint:'Liste des constact(s) associé(s) au client',
                    label:`Contact(s) associé(s)`,
                    },
                },
                produits:{ 
                    assignement : "hasMany", modelName : "Produit", 
                    sqlOptions:{foreignKey: 'fourniseurId', onDelete: "CASCADE",},
                    infos:{ 
                        hint:'Liste des produit(s) associé(s) au fournisseur',
                        label:`produits(s) associé(s)`,
                    },
                },
            },
        },

    },
}