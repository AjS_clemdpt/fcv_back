'use strict'
import EntityLayout from '../modules/ref_tools2/EntityLayoutBuilder.mjs'
import FcvRef from "./fcv.ref.js"

//creation de la couche entity
const entityLayout = new EntityLayout({
	sequelizeArgs: [
		process.SETTINGS.DATABASE.NAME,
		process.SETTINGS.DATABASE.USER,
		process.SETTINGS.DATABASE.PASSWORD,
		process.SETTINGS.DATABASE.OPTIONS,
	]
});

// chargement du schéma de l'application
entityLayout.loadSchema(FcvRef);


export default entityLayout 
