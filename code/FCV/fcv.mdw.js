'use strict'
//import { DALRequest , DALResponse } from "../modules/ref_tools/lib/DALCommunication.mjs";
import { Router } from 'express';
import path from 'path';
import { fileURLToPath } from 'url';

const viewPath = path.dirname(fileURLToPath(import.meta.url))+"/views/";


const routeur = new Router();
routeur.get('/endPoint', async function (req, res) {
	//res.render(__dirname + '/views/endPoint.ejs')
})
//.post('/:action', upload.array('files'), async function (req, res) {

const mdw404 =	function(req, res, next) {
		res.status(404);
		console.warn(`Page ${req.url} not found`)
		
		// respond with html page
		if (req.accepts('html')) {
			res.render(viewPath+'404.ejs', { url: req.url });
		}

		// respond with json
		else if (req.accepts('json')) {
			res.json({ error: 'Not found' });
		}

		// default to plain-text. send()
		else 
			res.type('txt').send('Not found');
	}

export default {
	mdw404,
	routeur,
} ;
