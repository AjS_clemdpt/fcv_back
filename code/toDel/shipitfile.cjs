const settings = import("./settings.js");
const fs = require('fs');
const path = require('path');
const { version: pkVersion, repository } = JSON.parse(fs.readFileSync('../../package.json', 'utf8'));
const consoleControl = require('console-control-strings');
const inquirer = require('inquirer');


const output = {
  settings: {
    activeDebug: false,
  },
  trace: function (txt) {
    console.log(consoleControl.color('blue') + txt + consoleControl.color('reset'))
  },
  debug: function (txt) {
    if (this.settings.activeDebug)
      console.log(consoleControl.color('italic','yellow') + '->' + txt + consoleControl.color('reset'))
  },
  error: function (txt) {
    console.log(consoleControl.color('red', 'bold') + '!!! ' + txt + ' !!!' + consoleControl.color('reset'))
  }
}
const options = {};

module.exports = shipit => {
  require('shipit-deploy')(shipit);
  require('shipit-shared')(shipit);

  const appName = 'hello';

  shipit.initConfig({
    default: {
      deployTo: '/var/www',
      workspace: '/var/www/',
      keepReleases: 2,
      repositoryUrl: repository.url,// 'https://git-provider.tld/YOUR_GIT_USERNAME/YOUR_GIT_REPO_NAME.git',
    },
    test: {
      servers: 'root@51.75.202.116',
    },
  });


  shipit.blTask('deploy', async () => {
    output.trace(`Bienvenue dans l'outil de déploiment.`)
    
    // pose la question de la version
    await inquirer.prompt({
      type: 'input',
      name: 'version',
      default: pkVersion,
      message: 'Indiquez la version à déployer:',
      validate: txt => !/^[\d]{1,2}[.][\d]{1,3}[.][\d]{1,3}$/.test(txt) ? 'format invalide' : true
    }).then(answers => {
      options.version = answers.version;
    });

    await shipit.remote('pwd');

    // copie de l'image sur le serveur distant
    await shipit.copyToRemote('Dockerfile','/root');

    /*
    // build de l'image
    output.trace(`Build l'image du projet en version ${options.version}`);
    let cmd = `docker build --pull --no-cache --rm -t fcvback:${options.version} --build-arg GITHUB_AUTHEN=${process.SETTINGS.DEPLOY.git.username}:${process.SETTINGS.DEPLOY.git.token} "."`;
    output.debug(cmd);
    await shipit.local(cmd);
*/
    // test de l'image
    /*deploy.task(`Test l'image du projet`, () => {
      deploy.trace('test en cours');
    });
    */
  });
}