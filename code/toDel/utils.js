// ajout la fonction merge à tous les objets
import merge from "lodash/merge.js"
import isFunction from "lodash/isFunction.js"

function sortProperty(object, fctCompare){
    return Object.fromEntries(Object.entries(object).sort(fctCompare ?? ((a,b) => a-b) )) ;
}

export { merge, isFunction, sortProperty}