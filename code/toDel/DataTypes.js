import { Sequelize } from 'sequelize';
import * as validator from "validator";
const DataTypes = Sequelize.DataTypes;

DataTypes.EMAIL = class EMAIL extends DataTypes.STRING {
    constructor() {
        super();
        this.key = 'EMAIL'
    }

    // Todo
    validate(value) {
        if (!validator.isEmail(value))
            throw new sequelizeErrors.ValidationError(`'${value}' is not a valid email`);
        return true;
    }

    // Optional: sanitizer
    //_sanitize(value) {return new IP(value);};
    
    //_isChanged(value, originalValue) {return !value.toBuffer().equals(originalValue.toBuffer());};
    
    // Optional: value stringifier before sending to database
    //_stringify(datetime) {return datetime.toBuffer();};
    
    // Optional: parser for values received from the database
    //static parse(value) {return Number.parseInt(value);}
}
DataTypes.PHONE = class TEL extends DataTypes.STRING {
    constructor() {
        super(15);
        this.length = 15;
        this.key = 'TEL';
    }

    // Todo
    validate(value) {
        if (!validator.isMobilePhone(value))
            throw new sequelizeErrors.ValidationError(`'${value}' is not a valid mobile phone.`);
        return true;
    }
}
DataTypes.CURRENCY = class CURRENCY extends DataTypes.DECIMAL {
    constructor() {
        super(10,2);
        this.key = 'CURRENCY'
    }
}

DataTypes.CODE = class CODE extends DataTypes.STRING {
    constructor() {
        super(50);
        this.allowNull = false;
        this.unique  = true;
        this.length = 50;
        this.key = 'CODE';
    }
}
DataTypes.LABEL = class LABEL extends DataTypes.STRING {
    constructor() {
        super(255);
        this.allowNull = false;
        this.unique  = true;
        this.length = 255;
        this.key = 'LABEL';
    }
}
DataTypes.CHOICE = class CHOICE extends DataTypes.ENUM {
    
}
DataTypes.CHOICES = class CHOICES extends DataTypes.ENUM {
    constructor(values) {
        this.choices = values;
        super(this.choices);
        this.key = 'CHOICES';
    }

    // Todo
    validate(values) {
        if( Array.isArray(values)){
            values.forEach(value => {
                if (!this.choices.include(value))
                    throw new sequelizeErrors.ValidationError(`'${value}' is not a valid possible choice.`);
            })
            if( this.min && values.length < this.min)
                throw new sequelizeErrors.ValidationError(`'You haven't enougth choice (${this.values.length}). Minimum is ${this.min}.`);
            if( this.max && values.length < this.max)
                throw new sequelizeErrors.ValidationError(`'You have too choice (${this.values.length}). Max is ${this.max}.`);
        }
        return true;
    }
}
export default DataTypes;