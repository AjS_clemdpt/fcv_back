import './settings.js.js';
import prompts from 'prompts';
import fs from 'fs';
import path from 'path';
import consoleControl from 'console-control-strings';
import { spawn } from 'child_process';
import { timeStamp } from 'console';
let numTask = 0;

const { version: pkVersion } = JSON.parse(fs.readFileSync(path.join(path.resolve(''), './../../package.json')));

const deploy = {
    trace: function (txt) {
        console.log(consoleControl.color('blue') + txt + consoleControl.color('reset'))
    },
    debug: function (txt) {
        if (this.options.activeDebug)
            console.log(consoleControl.color('italic') + '->' + txt + consoleControl.color('reset'))
    },
    error: function (txt) {
        console.log(consoleControl.color('red', 'bold') + '!!! ' + txt + ' !!!' + consoleControl.color('reset'))
    },
    task: function (title, actions) {
        try {
            this.trace(`${(++numTask)} : "${title}" start ...`);
            actions();
        } catch (error) {
            console.error(error);
        }
    },
    exec: function (cmd, args) {
        this.debug(cmd + ' ' + args.join(' '));
        const process = spawn(cmd, args);
        process.stdout.on("data", data => {
            this.trace(`stdout: ${data}`);
        });

        process.stderr.on("data", data => {
            this.debug(`stderr: ${data}`);
        });

        process.on('error', (error) => {
            this.error(`error: ${error.message}`);
        });

        process.on("close", code => {
            this.trace((code === 0) ? `` : `child process exited with code ${code}`);
        });
        /*
        , (error, stdout, stderr) => {
            if (error) {
                this.error(`exec error: ${error}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
            console.error(`stderr: ${stderr}`);
        });
        */
    },
};

(async () => {
    deploy.trace(`Bienvenue dans l'outil de déploiment.`)

    deploy.options = await prompts([
        // pose la question de l'environnement
        {
            type: 'select',
            name: 'env',
            message: 'Selectionnez l\'environnement de déploiement:',
            //initial : 'test',
            choices: [
                { title: 'test', value: 'test' },
                { title: 'prod', value: 'prod' },
                { title: 'demo', value: 'demo' },
            ],
        },
        // pose la question de la version
        {
            type: 'text',
            name: 'version',
            message: 'Indiquez la version à déployer:',
            initial: pkVersion,
            //validate: txt => !/^[\d]{1,2}[.][\d]{1,3}[.][\d]{1,3}$/.test(txt) ? 'format invalide' : true
        }
    ]);
    deploy.options.activeDebug = false;

    // build de l'image
    deploy.task(`Build l'image du projet en version ${deploy.version}`, () => {
        deploy.exec(`docker`, ['build', '--pull', '--rm', '-f', "./Dockerfile"
            , '-t', `fcvback:${deploy.version}`
            , '--build-arg', `GITHUB_AUTHEN=${process.SETTINGS.DEPLOY.username}:${process.SETTINGS.DEPLOY.token}`
            , '.',
        ]
            , { encoding: 'utf8' }
        );
    });
    // test de l'image
    deploy.task(`Test l'image du projet`, () => {
        deploy.trace('test en cours');
    });
    
   /*
  
  //se connecte en ssh sur le bon environnement en demandant le password
  ssh ...
  // on push l'image crée en locale vers le server ssh
  ?????
  // on coupe l'image précédente
  ????
  // on fait un backup de la BDD de la version précédente
  ????
  // on charge le backup de la BDD dans la nouvelle version
  ????
  // on déplace le backup au bon endroit
  ?????
  // on lance le conteneur
  docker 
  // on test la nouvelle image
  ???
  // on se déconecte du ssh
  logout 
*/
})();
