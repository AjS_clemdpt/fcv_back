'use strict'
// securité : https://leapgraph.com/graphql-api-security#set-query-timeouts 
// https://www.robinwieruch.de/graphql-apollo-server-tutorial#postgresql-with-sequelize-for-a-graphql-server
import "./settings.js";
import express from 'express';
import bodyParser from "body-parser";
import cors from 'cors';
import endPointMdw from './DAL/index.js';
import Auth,{authReferentiel} from './Authentification/index.js';
import entityLayout from "./FCV/index.js"
import FcvMdw from "./FCV/fcv.mdw.js"

const app = express();
// ajout le EntityLayout 
app.entityLayout = entityLayout;

//Body Parser
//https://medium.com/@axel.marciano/votre-premi%C3%A8re-application-en-react-node-express-mongodb-5ab0dc531091
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())

// log la demande
app.use((req,res,next)=>{
  console.debug(`URL demandée : ${req.url}`)
  next();
})

//-- module authentification : passport
app.entityLayout.loadSchema(authReferentiel)
app.use('/Auth/',Auth(app))

//-- ajout le middleware d'écoute du EntityLayout
app.use('/DAL/',endPointMdw)

//-- gestion page 404
app.use(FcvMdw.mdw404)

//-- lance sequelize une fois que tous les schémas ont pu être chargés
app.entityLayout.launchSequelize(process.SETTINGS.DATABASE.SYNC_OPTIONS);

// lancement du serveur
app.listen(process.SETTINGS.SERVER_WEB.PORT, () =>
  console.log(`🚀 Server ready at http://localhost:${process.SETTINGS.SERVER_WEB.PORT}/`)
);

