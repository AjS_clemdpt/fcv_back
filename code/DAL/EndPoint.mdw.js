'use strict'
//import { DALRequest , DALResponse } from "../modules/ref_tools/lib/DALCommunication.mjs";
import { Router } from 'express';
import CommandInterpretor from './CommandInterpretor.js';
import path from 'path';
import { fileURLToPath } from 'url';
import multer  from 'multer';

const upload  = multer({
  //dest: 'uploads/', 
  storage: multer.memoryStorage(),
  limits : { fileSize: (200*1024*1024) }, //200 Mo
})

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const Routeur = Router()
let cmdInterpretor =null

// TODO : prevoir routes pour administration de sequelize ...
Routeur
  .get('/endPoint', async function (req, res) {
    res.render(__dirname + '/views/endPoint.ejs')
  })
  .post('/:action', upload.array('files'), async (req, res) => {
    let response  ={
      isValid:false,
    };
    try {
      const action =  req.params.action;
      if (!action) throw new Error(`Aucune action de parsée`)
      if(!cmdInterpretor) cmdInterpretor = new CommandInterpretor(req.app.entityLayout)
      if( typeof cmdInterpretor[action] !== 'function' ) throw new Error(`L'action ${action} n'existe pas !`)

      // lecture de l'appel
      let msg = req.body;
      
      // rehydrate l'item si des fichiers sont présent
      if( req.body.msg && req.headers['content-type'].indexOf('multipart/form-data') >-1){
        msg = JSON.parse(req.body.msg)

      if(req.files?.length )
        req.files.forEach( e=> {
          console.debug(`Réhydration du champ ${e.originalname}.`)
          let target = msg
          const properties = e.originalname.split('.')
          for( var i=0; i< properties.length-1;i++){
            target = target[properties[i]]
          }
          target[properties[i]] = e.buffer;
        });
      }

      // lancement du command Interpretor
      response.isValid = true
      response.msg = await cmdInterpretor[action](msg)
    } 
    catch (ex) {
      console.error(ex)
    }

    // on supprime systématiquement les fichiers uploadés 
    try{

    }
    catch (ex) {
      console.error(ex)
    }

    //renvoi du resultat
    res.json(response);
  })
;

export default Routeur;
