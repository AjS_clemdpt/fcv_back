//https://sequelize.org/master/manual/eager-loading.html
//https://sequelize.org/master/class/lib/model.js~Model.html#static-method-findOne

export default class CommandInterpretor {

  constructor(entityLayout) {
    this.entityLayout = entityLayout
  }
  /*
  #getModel(entityName){
    if (!entityName) throw new Error(`La propriété entityName est vide.`)
    const model = this.entityLayout.dataModels[entityName]
    if( !model) throw new Error(`Impossible de trouver l'entity ${entityName} dans le référentiel.`)
    return model;
  }
  
  #prepareFindOptions(model,findOptions){
    // on transforme les models des includes de string en object Model
    if(findOptions?.include){
      if(!Array.isArray(findOptions.include))findOptions.include = [findOptions.include]
      findOptions.include.map(e => {
        if( typeof e.model == "string" ) 
          e.model = this.#getModel(e.model);
      })
    }
    /*
    // on include automatiquement toutes les associations disponibles
    for( const key in model.associations){
      findOptions.include.push({
          model: model.associations[key].source,
          as: model.associations[key].as}
      )
    }
    * /
  }
  */

  getReferentiel(opt) {
    return this.entityLayout.referentiel;
  }
  async findAll(opt) { 
    opt = this.entityLayout.completeModel(opt)
    const response = await opt.model.findAndCountAll(opt.findOptions)
    return {
      count : response.rows.length,
      data : response.rows,
    }
  }
  async findOne(opt) { 
    opt = this.entityLayout.completeModel(opt)

    return {
      data : await opt.model.findOne(opt.findOptions),
      count:1,
    }
  }
  async save(opt) {
    opt = this.entityLayout.completeModel(opt)
    if( !opt.item) throw new Error(`Impossible de trouver un objet à sauvegarder dans la propriété item.`);
    
    return {
      data : await new opt.model().fillAndSave(opt.item),
      count:1,
    }
  }
  async destroy(opt) {
    const seqObj = await this.findAll(opt)
    if( seqObj.count == 0) throw new Error(`Impossible de trouver un objet à sauvegarder dans la propriété item.`);
    const response = await seqObj.data.destroy(opt);
    return  {
      count:response.rows.length, 
      data: response.rows,
    }

  }
}
