import {merge} from "@anjousoft/tools/utils.mjs";

const settings = {
  default : {
    SERVER_WEB : {
      PORT : 80, // numéro du PORT où le serveur doit écouter
    },
    DEPLOY : {
      username  : "root" , //utilisateur sur la machine distante
      host      : '51.75.202.116', // adresse du serveur
      git :{
        username  : 'deployTool', //username de Git sur serveur
        token     : 'CdPvDm1mAAm9StPYdMML',  //token de partage pour récupérer les sources
      },
    },
    DATABASE: {
      NAME : "fcvDb",
      USER: "fcvUser",
      PASSWORD: "fcvback49170",
      OPTIONS : {
        host    : "10.0.0.10",
        port    : "5432",
        dialect:   "postgres",
        //logging: (msg)=> console.log(msg) ,
      },
      SYNC_OPTIONS : {
        alter:true,
        //forceSync:true,
        //logging: true,
      },
      SGS: {
        /**
         * update modes when sending nested association objects
         * PARTIEL > Les élément déjà existant en BDD et non-présents dans la listes sont ignorés 
         * TOTAL > Les élément  déjà existant en BDD et non-présents dans la listes sont supprimés 
        */
        nestedUpdateMode: 'TOTAL',  //https://snyk.io/advisor/npm-package/sequelize-graphql-schema
        nestedMutations: true,
        limits: {
          nested:true,
        }
      }
    }

  },
  prod : {
    SERVER_WEB : {
      PORT : process.env.PORT || 8080,
    },
    DEPLOY : {
      username  : 'root',
      host      : '51.75.202.116',
      git :{
        username  : 'deployTool',
        token     : 'CdPvDm1mAAm9StPYdMML',  
      },
    },
    DATABASE: {
      NAME : "fcvDb",
      USER: "fcvUser",
      PASSWORD: "fcvback49170",
      OPTIONS : {
        host    : "10.0.0.10",
        port    : "5432",
        dialect:   "postgres",
        //logging: (msg)=> console.log(msg) ,
      },
      SYNC_OPTIONS : {
        alter:true,
        //forceSync:true,
        //logging: true,
      },
      SGS: {
        nestedUpdateMode: 'MIXED',  //https://snyk.io/advisor/npm-package/sequelize-graphql-schema
        nestedMutations: true,
        limits: {
          nested:true,
        }
        //dataloader: true,
        //dataloader: true,
      }
    }

  },
  dev: {
    SERVER_WEB : {
      PORT : 4000,
    },
    DATABASE: {
      //PASSWORD: "fcvback49170",
      OPTIONS : {
        host    : "localhost",
      },
      SYNC_OPTIONS : {
        force: false 
      },
    },
  },
  test: {
    DATABASE: {
      PASSWORD: "fcvback49170"
    }
  },
};

let env = process.env.NODE_ENV || "prod";
process.SETTINGS = merge( settings[env], settings.default ) ?? {};
process.SETTINGS.ENV = env;

export default process.SETTINGS;
