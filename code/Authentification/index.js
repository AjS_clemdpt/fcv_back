'use strict'
//https://www.passportjs.org/docs/
//https://github.com/jaredhanson/passport-local
//https://softwareontheroad.com/nodejs-jwt-authentication-oauth/
//https://stackoverflow.com/questions/60434485/nuxt-auth-with-passportjs => redirection nuxt

import passport from "passport"
import session  from "express-session"
import {Strategy as LocalStrategy } from "passport-local"
import { Router } from 'express';

export default function(app){
	const routeur = Router();
	
	app.use(session({ 
		secret: 'keyboard cat', 
		resave: false, 
		saveUninitialized: false 
	}));
	
	passport.use(new LocalStrategy({
		},
		async (username, password, done) => {
			try{
				const userModel = app.entityLayout.dataModels["User"];
				if(!userModel) throw new Error("Le model User est inexistant.")
				
				let query = app.entityLayout.completeModel({
					attributes: ['civilite', 'email','id','entreprise','nom','prenom','tel1','password'],
					//include:['profils',],
					include:[{
						model : "Profil",
						as : 'profils',
						attributes: ['code','label'],
						include: {
							model : "Action",
							as : "actions",
							attributes: ['code'],
						},
					}],
					where:{
						email: username,
					},
				})

				let userData = await userModel.findOne(query)
				if(!userData) throw new Error(`Aucun utilisateur trouvé pour l'utilisateur ${username}.`)
				
				// transforme l'objet sequelize en plain object
				userData = userData.get({ plain: true })
				
				//verifyPassword
				if( userData.password != password ) throw new Error(`Le mot de passe de l'utilisateur est faux.`)
				delete userData.passport
				
				return done(null, userData);
	
			}catch(err){
				return done(err, null);
			}
		}
	));
	passport.serializeUser(function(user, cb) {
		console.log("serializeUser");
	});
	passport.deserializeUser(function(id, cb) {
		console.log("deserializeUser");
	});
	app.use(passport.initialize());
	app.use(passport.session());


	routeur.post('/login', (req, res, next) => {
		return passport.authenticate("local", (err = {}, token) => {
			if (err || !token) {
				return res.status(403).send({message: err.message, stack: err.stack});
			}
	
			// Sign the user
			req.login(token, { session: false }, (err) => {
				if (err) {
					// It should not happen
					return res.status(200).send({ err });
				}
				return res.status(200).send({ token });
			});
		})(req, res, next);

		/*
		passport.authenticate('local',{ 
			session: false,
			failureRedirect: '/loginFail',
		},
		function(user, userData, failure, statut){
			if (failure) return res.json({ failure });
			
			console.log(`Authentification réussie !`)
				const token = jwt.sign(req.user.userId, 'JWT_SECRET_OR_KEY');
			res.json(token);
		})
		*/
		}
	);

	return routeur;
}

import authReferentiel from "./authen.ref.js"
export {authReferentiel}
