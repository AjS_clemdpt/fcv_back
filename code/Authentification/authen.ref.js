//https://sequelize.org/master/manual/model-basics.html

export default {
  models : {
    User: {
      model: {
        nom: { 
          datatype: {type: "STRING", length:50, allowNull: false, validate: { notEmpty: true, }, }, 
        },
        prenom: { 
          datatype: {type: "STRING", length:50, allowNull: false, validate: { notEmpty: true, }, }, 
        },
        email: { 
          datatype: {type: "EMAIL", unique: true, }, 
        },
        actif: { 
          datatype: {type: "BOOLEAN", allowNull: false, defaultValue: true }, 
        },
        password: { 
          datatype: {type: "PASSWORD"}, 
        },
        
      },
      associate: {
        profils:{ assignement : "belongsToMany", modelName : "Profil",
          sqlOptions:{foreignKey: 'idUser', through: "J_UserProfil", onDelete: "CASCADE"},
          infos:{ 
            hint:'Liste des profil(s) associée(s) à cet utilisateur',
            label:`Profil(s) associé(s)`,
          },
        },
      }
    },
    J_UserProfil:{model:{},},
    
    Profil: {
      model: {
        code: { datatype: {type: "CODE" },},
        label: { datatype: {type: "LABEL" },},
        description: { datatype: {type: "STRING" },},
      },
      associate: {
        users:{ assignement : "belongsToMany", modelName : "User", 
          sqlOptions:{foreignKey: 'idProfil', through: "J_UserProfil", onDelete: "CASCADE"},
          infos:{ 
            hint:'Liste des utilisateurs associés à ce profil',
            label:`Utilisateur(s) associé(s)`,
          },
        },
        actions:{ assignement : "belongsToMany", modelName : "Action", 
          sqlOptions:{ 
            foreignKey: 'idProfil', through: "J_ProfilAction"  //, onDelete: "CASCADE"
          },
          infos:{ 
            hint:'Liste des action(s) associée(s) à ce profil',
            label:`Action(s) associée(s)`,
          },
        },
      }
    },
    J_ProfilAction:{model:{},},
    Action: {
      model: {
        code: { 
          datatype: {type: "CODE" },
        },
        label: {
          datatype: {
            type: "LABEL",
            defaultValue: "",  // defaultValue? : string
            align: "start", //align?: 'start' | 'center' | 'end',
            sortable: true, //sortable?: boolean,
            //filterable?: boolean,
            //groupable?: boolean,
            //divider?: boolean,
            //class?: string | string[],
            //cellClass?: string | string[],
            //width?: string | number,
            //filter?: (value: any, search: string, item: any) => boolean,
            //sort?: (a: any, b: any) => number
          },
          infos:{
            text: "Label des actions",
            hide: false,   // hide? : boolean
            hint: "Cette colonne définie le label d'une action.", //hint? : string
            placeHolder: "renseigner le libellé ici", // placeHolder? : string
          },
        },
      },
      associate: {
        profils:{ assignement : "belongsToMany", modelName : "Profil", 
          sqlOptions:{foreignKey: 'idAction', through: "J_ProfilAction", onDelete: "CASCADE"},
          infos:{ 
            hint:'Liste des profil(s) associée(s) à cette action',
            label:`Profil(s) associé(s)`,
          },
        },
      },
    },
  }
}
