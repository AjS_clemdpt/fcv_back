INSERT into public."Users" (id,nom, prenom, "createdAt","updatedAt","createdById") VALUES (1,'aubert','clement',now(),now(),1);

INSERT into public."Actions" ( code, label, "createdAt","updatedAt", "createdById") values ('ROLE_INITIATEUR'        ,'Donne accès à l''initilisation des fiches chantier ' ,now(),now(),1);
INSERT into public."Actions" ( code, label, "createdAt","updatedAt", "createdById") values ('ROLE_SUIVEUR'           ,'Donne accès au suivi des fiches chantier'           ,now(),now(),1);
INSERT into public."Actions" ( code, label, "createdAt","updatedAt", "createdById") values ('ROLE_PREPARATEUR'       ,'Donne accès au fiche "en fabrication"'            ,now(),now(),1);
INSERT into public."Actions" ( code, label, "createdAt","updatedAt", "createdById") values ('ROLE_PLANIFICATEUR'     ,'Donne accès au fiche "planifiable"'               ,now(),now(),1);
INSERT into public."Actions" ( code, label, "createdAt","updatedAt", "createdById") values ('ROLE_RESPONSABLE _POSE' ,'Donne accès au fiche "possable"'                  ,now(),now(),1);
INSERT into public."Actions" ( code, label, "createdAt","updatedAt", "createdById") values ('ROLE_COMPTABLE'         ,'Donne accès au fiche "facturable"'                ,now(),now(),1);
--select * from public."Actions"
